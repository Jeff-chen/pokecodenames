class AddPhasesToGame < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :current_color_turn, :string
    add_column :games, :current_phase, :string
    add_column :games, :current_clueword, :string
    add_column :games, :current_guesses_remaining, :integer
  end
end
