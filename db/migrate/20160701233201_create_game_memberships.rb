class CreateGameMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :game_memberships do |t|
      t.references :game, foreign_key: true
      t.references :user, foreign_key: true
      t.string :role
      t.string :team_color

      t.timestamps
    end
  end
end
