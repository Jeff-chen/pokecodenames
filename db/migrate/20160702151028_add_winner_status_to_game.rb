class AddWinnerStatusToGame < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :victor, :string
  end
end
