class CreateGameGuesses < ActiveRecord::Migration[5.0]
  def change
    create_table :game_guesses do |t|
      t.references :game, foreign_key: true
      t.integer :slot
      t.string :true_color
      t.integer :pokemon_id
      t.boolean :guessed

      t.timestamps
    end
  end
end
