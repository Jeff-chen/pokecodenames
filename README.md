This code repository is a proof of concept adaptation of the board game Codenames that uses Rails 5 ActionCable/sockets to test making a webapp that can be played in real time using the standard Rails framework.

Caveats: I started off an ActionCable tutorial showing how to implement a chat room so some remnants of that code are still lurking. (e.g. chatroom model) Also, I was writing most of this in a "hackathon" like environment where I was getting as much of it done in 24 hours, so I neglected automated tests, but normally I am much better at it.

Pokemon codenames can be played at this link: http://pokecodenames.herokuapp.com and since it is a sparse, side project, there are still many incomplete things/bugs. 

To play, 4 players are needed to sign on with different names. A user will log on providing a username (no password needed - this means that users could be impersonated but it is not worth adding another password to everyone's lives to remember) and then after logging on, see a list of available games or create a new one. Once a player clicks a link to a game, they must join the game (if there are spots left) by selecting a role, or if left blank, being assigned one at random. Once 4 players have joined a game in this manner, play begins with the red spymaster giving a clue.
