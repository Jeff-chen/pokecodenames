module GamesHelper
  def zomg
    if @current_user && @game && @game.game_memberships.map(&:user).include?( @current_user)
      return "You are a #{@current_user.role_name_in_game(@game)}!"
    end
  end
end
