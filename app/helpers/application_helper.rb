module ApplicationHelper
  def oinky_poo
    if @current_user
      return 'log out here'
    else
      return 'log in to play here'
    end
  end
end
