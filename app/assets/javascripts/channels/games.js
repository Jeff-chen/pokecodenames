
App.messages = App.cable.subscriptions.create('GamesChannel',{
  //received: data is like a poke_id followed by a color.
  received: function(data) {
    //alert(data);
    if(data.hello === 'hello'){
      alert("hello hello");
    }
    
    if(data.action === 'change_color'){
      $('#poke_'+data.poke_id).addClass(data.color);
    }
    if(data.action === 'toggle_clues'){
      $('form.edit_game input').prop('disabled', data.status == 'off');
    }
    if(data.action === 'toggle_guesses'){
      $('table#lolgrid input').prop('disabled', (data.status === 'off'));
    }
    if(data.action === 'notify'){
      $('#messages').append(this.renderMessage(data.message));
    }
    //alert(data.action);
    if(data.action == 'changespan'){
      //alert(data.spanname);
      var spanname = data.spanname;
      var it = "span." + spanname;
      $(it).html(data.message); //Sanitize this! don't want <script>alert('hi')</script>
    }
  },
  
  renderMessage: function(data) {
    return "<p>" + data+ "</p>";
  }
});
