class SessionsController < ApplicationController
  def new
    
  end
  def create
    user = User.find_or_create_by(username: params[:username])
    session[:user_id] = user.id
    cookies.signed['user.id'] = user.id
    redirect_to root_path
  end
  
  def destroy
    session[:user_id] = nil
    cookies.signed['user.id'] = nil
    redirect_to '/login'
  end
end
