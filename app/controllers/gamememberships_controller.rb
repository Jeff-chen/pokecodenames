class GamemembershipsController < ApplicationController

  def create
    @game = Game.find(params[:game_id])
    if GameMembership.where(user: @current_user, game:@game).size > 0
      redirect_to game_path(@game), :notice => "You're in this game!"
    elsif @game.ready?
      redirect_to game_path(@game), :notice => "This game is full!"
    else
      requested = params[:requested_opt].split("_")
      requested_col, requested_role = requested[0], requested[1]
      gm = @game.enroll_user(@current_user, {:color => requested_col, :role => requested_role})
      if @game.ready?
        ActionCable.server.broadcast("poke_poke_#{@game.red_master_user.username}",{"action": "toggle_clues", "status": "on"})
      end
      redirect_to game_path(@game), :notice => "Success! You are enrolled as #{gm.team_color} #{gm.role}."
    end
  end
end
