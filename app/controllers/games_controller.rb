class GamesController < ApplicationController
  #after_filter :check_privs, :only => [:show, :update, :create]
  def index
    if @current_user
      @games = Game.where(:victor => nil)
    end
  end
  def new
    @game = Game.new
  end
  
  def update
    @game = Game.find(params[:id])
    
    things_to_bcast = @game.process_with_user(@current_user, params)
    logger.debug things_to_bcast.inspect
    things_to_bcast.each{|x, y| ActionCable.server.broadcast x, y}
  end
  
  def create
    @game = Game.create!(:description => params[:description])
    redirect_to game_path(@game)
  end
  
  def show
    @game = Game.find(params[:id])
    #check_privs
    if @game.current_phase == 'master'
      #if @current_user.
    end
    check_privs
  end
  
  private
  
  def check_privs
    if !@current_user
      @allow_guess = "disabled"
      return nil
    end
    if @current_user.is_master_in_game?(@game)
      @clueword_form = true
      @grid_priv = true
      @show_form = false
      if (@game.current_color_turn != @current_user.color_in_game(@game)) || @game.current_phase == 'guesser'
        @enabled_buttons =  "disabled"
      end
    else
      if !@current_user.is_master_in_game?(@game) && (@game.current_phase != 'guesser' || (@game.current_color_turn != @current_user.color_in_game(@game)))
        @allow_guess = "disabled"
      end
    end
  end
end
