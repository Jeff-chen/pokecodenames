class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :grab_user
  def grab_user
    if session[:user_id]
      @current_user = User.find(session[:user_id])
    end
  end
  
end
