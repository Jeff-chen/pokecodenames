class MessagesController < ApplicationController
  def create
    message = Message.new(content: message_params[:content], chatroom_id: message_params[:chatroom_id])
    
    message.user = @current_user#User.find(message_params[:current_user_id])
    if message.save
      ActionCable.server.broadcast 'messages',
        message: message.content,
        user: message.user.username
      head :ok
    end
  end

  private

    def message_params
      params.require(:message).permit(:content, :chatroom_id, :current_user_id)
    end
end
