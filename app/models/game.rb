class Game < ApplicationRecord
  POKEMON_SPECIES = 718
  TOTAL_GRID = 5*5
  RED_GUESSES = 9
  BLUE_GUESSES = 8
  
  COLORS=['red','blue']
  ROLES=['guesser','master']
  
  after_create :create_initial_array, :set_turn_to_red
  has_many :game_guesses
  has_many :game_memberships
  
  def available_opts
    opts = []
    COLORS.each do |c|
      ROLES.each do |r|
        opts << "#{c}_#{r}" if GameMembership.where(game:self, team_color:c, role:r).size == 0
      end
    end
    return opts
  end
  
  def enroll_user(user, opts={})
    color = opts.delete(:color) || COLORS.sample
    role = opts.delete(:role) || ROLES.sample
    until GameMembership.where(game:self, team_color:color, role:role).size == 0
      color = Game::COLORS.sample
      role = Game::ROLES.sample
    end
    gm = GameMembership.create!(user:user, game:self, role:role,team_color:color)
    return gm
  end
  
  def has_user?(user)
    GameMembership.where(game:self, user:user).size > 0
  end
  
  def ready?
    game_memberships.size >= 4
  end
  
  
  def create_initial_array
    poke_ids = 1.upto(POKEMON_SPECIES).to_a.sample(TOTAL_GRID)# gets pokemon ids
    
    totes = 1.upto(TOTAL_GRID ).to_a
    reds = totes.sample(RED_GUESSES)# gets red team
    totes -= reds
    blues = totes.sample(BLUE_GUESSES)
    totes -= blues
    black = totes.sample(1)
    
    poke_ids.each_with_index do |poke_id, i|
      my_i = i+1
      if reds.any?{|z| my_i == z}
        color = 'red'
      elsif blues.any?{|z| my_i == z}
        color = 'blue'
      elsif black.first == my_i
        color = 'black'
      else
        color = 'gray'
      end
      gg = GameGuess.new(game: self, true_color: color, pokemon_id: poke_id, slot: my_i, guessed: false)
      gg.save!
    end
  end
  
  def red_master_user
    valid_gms = game_memberships.select{|i| i.role == 'master' && i.team_color == 'red'}
    valid_gms.size == 1 ? valid_gms[0].user : ''
  end
  
  def process_with_user(current_user, params)
    message = ''
    queues = []
    notifications = []
    if ready? && current_user.color_in_game(self) == current_color_turn
      if current_phase == 'master' && current_user.is_master_in_game?(self) and params[:guesses] and params[:guesses].to_i > 0
        
        update_attributes(:current_clueword => params[:clueword], :current_guesses_remaining => params[:guesses], :current_phase => 'guesser')
        
        message += "Clue is: #{current_clueword} with guesses: #{current_guesses_remaining}"
        notifications << {"spanname": 'clueword', 'message': current_clueword}
        notifications << {"spanname": 'guessesremaining', 'message': current_guesses_remaining.to_s}
        queues << ["poke_poke_#{current_user.username.downcase}", {'action': 'toggle_clues', 'status': 'off'}]
        next_user_name = game_memberships.select{|gm| gm.team_color == current_user.color_in_game(self) && gm.role == 'guesser'}.first.user.username
        notifications << {"spanname": "currentcolor", 'message': "#{current_user.color_in_game(self)} #{current_phase}"}
        queues << ["poke_poke_#{next_user_name.downcase}", {'action': 'toggle_guesses', 'status': 'on'}]
      else
        if current_phase == 'guesser' && !current_user.is_master_in_game?(self)
          @gm = GameGuess.where(game: self, pokemon_id: params[:poke_id].to_i).first
          @gm.guessed = true
          @gm.save
          other_color = current_color_turn == Game::COLORS.first ? Game::COLORS.last : Game::COLORS.first
          all_users_usernames.each do |dude|
            queues << ["poke_poke_#{dude.downcase}", {'action': 'change_color', 'poke_id': params[:poke_id], 'color': @gm.true_color}]
          end
          if @gm.true_color == 'black'
            update_attributes(:victor => other_color)
            message += "#{other_color} team is the winner!"
          elsif check_winner
            update_attributes(:victor => check_winner)
            message += "#{check_winner} team is the winner!"
            notifications << {"spanname": "whoadude", 'message': '#{check_winner} team is the winner!'}
          end
          guesses = current_guesses_remaining - 1
          update_attributes(:current_guesses_remaining => guesses)
          message += "Guesses remaining: #{guesses}"
          notifications << {"spanname": 'guessesremaining', 'message': current_guesses_remaining.to_s}
          #-1 guesses is a free guess
          if current_guesses_remaining < 0 || (current_user.color_in_game(self) != @gm.true_color)
              current_phase = 'master'
              next_user_name = game_memberships.select{|gm| gm.team_color != current_user.color_in_game(self) && gm.role == 'master'}.first.user.username
              queues << ["poke_poke_#{current_user.username.downcase}", {'action': 'toggle_guesses', 'status': 'off'}]
              queues << ["poke_poke_#{next_user_name.downcase}", {'action': 'toggle_clues', 'status': 'on'}]
              #broadcast to next master to enable clues
              update_attributes(:current_color_turn => other_color, :current_phase => 'master')
              notifications << {"spanname": "currentcolor", 'message': "#{other_color} #{current_phase}"}
          end
        end
      end
      
      all_users_usernames.each do |dude|
        notifications.each do |note|
          queues << ["poke_poke_#{dude.downcase}", {'action': 'changespan', 'spanname': note[:spanname], 'message': note[:message].gsub("\>",'').gsub("\<", '')}]
        end
        #queues << ["poke_poke_#{dude}", {'action': 'notify', 'message': message}]
      end
    end
    logger.debug queues.inspect
    return queues
  end
  
  def check_winner
    return victor if victor
    
    thing = nil
    COLORS.each do |col|
      if game_guesses.select{|gg| gg.guessed == true && gg.true_color == col}.size == game_guesses.select{|gg| gg.true_color == col}.size
        thing = col
      end
    end
    return thing
  end
  
  def set_turn_to_red
    update_attributes(:current_color_turn => COLORS.first, :current_phase => ROLES.last)
  end
  
  def alternate_color_turn
    #todo: use update attributes
    if current_color_turn == 'blue'
      current_color_turn = 'red'
    else
      current_color_turn = 'blue'
    end
  end
  
  def renew_game
    game_guesses.each{|i| i.guessed = false; i.save}
    set_turn_to_red
    save
  end
  
  def all_users_usernames
    game_memberships.map{|gm| gm.user.username}
  end
  
  def alternate_phase
    #todo: use update attributes
    if current_phase == 'master'
      current_phase = 'guesser'
    else
      current_phase = 'master'
    end
  end
end
