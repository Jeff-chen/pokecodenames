class GameGuess < ApplicationRecord
  belongs_to :game
  
  def display_class
    guessed ? true_color : ""
  end
end
