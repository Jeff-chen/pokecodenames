class User < ApplicationRecord
  has_many :messages
  has_many :chatrooms, through: :messages
  
  def is_master_in_game?(game)
    game.game_memberships.select{|gm| gm.role == 'master' and gm.user == self}.size > 0
  end
  
  def color_in_game(game)
    game.game_memberships.select{|gm| gm.user == self}.first.team_color
  end
  
  def role_in_game(game)
    game.game_memberships.select{|gm| gm.user == self}.first.role
  end
  
  def role_name_in_game(game)
    "#{color_in_game(game)} #{role_in_game(game)}"
  end
end

