class GamesChannel < ApplicationCable::Channel 
  def subscribed
    stream_from "poke_poke_#{current_user.downcase}"
  end
end