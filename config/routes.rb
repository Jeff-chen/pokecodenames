Rails.application.routes.draw do
  get 'gamememberships/create'

  get 'games/show'

  mount ActionCable.server => '/cable'

  
  #get '/games/new', to: 'games#new'
  #get '/games/:id', to: 'games#show'
  resources :games do
    post 'enroll' => 'gamememberships#create'
  end
  
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  
  get 'logout' => 'sessions#destroy'
  post 'logout' => 'sessions#destroy'
  
  root :to => 'games#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
